/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persistence.entities.Personas;

@Path("clientes")

public class ClientesRest {
     EntityManagerFactory emf= Persistence.createEntityManagerFactory("my_persistence_unit");
    EntityManager em;  
    
   @GET
   @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo(){
        em= emf.createEntityManager();
        List<Personas> lista =em.createNamedQuery("Personas.findAll").getResultList();
     
        return Response.ok(200).entity(lista).build();
    }
    
   @GET
   @Path("/{idbuscar}")
   @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar){
        em= emf.createEntityManager();
       Personas persona =em.find(Personas.class, idbuscar);
     
        return Response.ok(200).entity(persona).build();
    }
    
   @POST
   @Produces(MediaType.APPLICATION_JSON)
    public String nuevo(Personas persona){
        em= emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(persona);
        em.getTransaction().commit();
        
    return "Persona Guardada";
     
    }
    
    
    @PUT
    public Response actualizar(Personas persona){
         em= emf.createEntityManager();
        em.getTransaction().begin();
        persona= em.merge(persona);
        em.persist(persona);
        em.getTransaction().commit();
       return Response.ok(200).entity(persona).build();
     

    }
    
   @DELETE
   @Path("/{iddelete}")
   @Produces(MediaType.APPLICATION_JSON)
      public Response eliminaId(@PathParam("iddelete") String iddelete){
         em= emf.createEntityManager();
        em.getTransaction().begin();
        Personas persona= em.getReference(Personas.class, iddelete);
        em.remove(persona);
        em.getTransaction().commit();
       return Response.ok("cliente eliminado").build();
     

    }
    
}
